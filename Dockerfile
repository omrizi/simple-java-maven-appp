# we will use openjdk 8 with alpine as it is a very small linux distro
FROM openjdk:8-jre-alpine3.9
 
# copy the packaged jar file into our docker image
ARG compiled_app_dir="./target/"
ARG app_name="my-app-1.0-SNAPSHOT.jar"
COPY  ./target/my-app-1.0-SNAPSHOT.jar /
 
# set the startup command to execute the jar
ENTRYPOINT ["java", "-jar", "/my-app-1.0-SNAPSHOT.jar"]