#!/bin/bash

# The script will build and run the java app inside a docker
# The script assumes that the Dockerfile is located in the same directory

# Build the docker from Dockerfile

docker build -t java_app_docker:latest .

# Check if the docker run succeed or not

if [[ ${?} == 0 ]]
then
  echo "Docker file has been build succesfully"
else
    echo "Docker file failed to build !"
fi

# Run the Docker in order to Check that the process is working fine

docker run -n java_app_test java_app_docker:latest

# Check if the docker run succeed or not

if [[ ${?} == 0 ]]
then
  echo "Docker file has been running succesfully"
else
    echo "Docker file failed to run !"
fi
